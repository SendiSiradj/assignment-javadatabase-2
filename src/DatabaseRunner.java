import java.util.Scanner;

public class DatabaseRunner {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int opsi;
        DatabaseManipulator dummy = new DatabaseManipulator();

        do{
            System.out.println("Pilih Program :\n1.Tampil Data\n2.Insert Data\n3.Update Data\n4.Hapus Data\n5.Cari Data");
            opsi = input.nextInt();

            if (opsi == 1 ) {
                dummy.cetakData();
            }
            else if (opsi == 2) {
                dummy.insertData();
            }
            else if (opsi ==
                    3){
                dummy.updateData();
            }
            else if (opsi == 4){
                dummy.hapusData();
            }
            else if (opsi == 5) {
                dummy.cariData();
            }

        }while (opsi == 1 || opsi == 2 || opsi == 3 || opsi == 4 || opsi == 5);
    }
}
